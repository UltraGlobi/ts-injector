
const compilerOptions = require("./tsconfig.json").compilerOptions;

module.exports = function (w) {
	process.env.WALLABY = "true";
	return {
		debug: true,
		env: {
			type:"node"
		},
		files: [
			"tsconfig.json",
			"package.json",
			"src/*.ts",
		],
		tests: [ "spec/*.spec.ts" ],
		testFramework: "jasmine",
		compilers: {
			"**/*.(j|t)s?(x)": w.compilers.typeScript(compilerOptions)
		},
		preprocessors: {
			"**/*.jsts": file => file.changeExt("js").content
		}
	}
}
