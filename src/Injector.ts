/**
 * Injector class file.
 * @license "MIT Licence"
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * @copyright Silvan Pfister 2019
 * @author Silvan Pfister
 * @version 1.0
 */
import  "reflect-metadata"

/**
 * Provides injection and resolution methods
 * 
 * To access the annotated services, use the constant `DefaultInjector` from this module.
 * 
 * @requires reflect-metadata
 * @example
 *	import Injector from "./Injector"
 *	...
 *		public someMethod(){
 *			let customInjector = new Injector();
 *			customInjector.settings.strict = true;
 *			// FooImplementation extends FooService
 *			customInjector.Inject(FooService, new FooImplementation());
 *			let fooInstance = customInjector.Resolve(FooService); // = the above FooImplementation
 *			// BarService's constructor requires a FooService: constructor(serv: FooService) ...
 *			// which is stored in a public get accessor: Foo
 *			let barInstance = customInjector.Resolve(BarService);
 *			fooInstance === barInstance.Foo // returns true
 *		}
 */
export class Injector{

	private services : Map<new(...args:any[]) => any,any> = new Map();

	/**
	 * Provides some settings for the injector
	 */
	public settings = {
		/** Whether to require injections for all services (throw exceptions for missing) or to build them dynamically
		 * 
		 * - true: require injections and throw if missing
		 * - false (default): attempt to build all instances using default values
		 */
		strict: false
	}

	/**
	 * Stores the provided service for use of the Resolve method
	 * 
	 * Automatically called by the `@Service` decorator
	 * @param service The type to use a shared resolution for
	 * @param instance An optionally defined instance of the `service` to resolve to
	 * @example
	 *	class PseudoInterface{
	 *		...
	 *	}
	 *	class Implementation extends PseudoInterface{
	 *		...
	 *	}
	 *
	 *	DefaultInjector.Inject(PseudoInterface, new Implementation())
	 *	---
	 *	DefaultInjector.Inject(PseudoInterface, null)
	 */
	public Inject<T>(service: (new(...args:any[])=>T), instance: T|null) {
		this.services.set(service, instance ? instance : this.Resolve(service))
	}

	/**
	 * Resolves the provided type and constructs it and it's dependencies recoursively
	 * @param type The type to resolve (with a constructor constraint)
	 * @returns An instance of the type
	 * @throws If settings.strict is true and a resolution with missing services is attempted
	 * @example
	 * 	(at)Service(123)
	 * 	class Foo{
	 * 		constructor(FooBar:number)
	 * 	}
	 * 	class Bar {
	 * 		constructor(FooBar:Foo){
	 * 			...
	 * 		}
	 * 	}
	 * 	let instance:Bar = DefaultInjector.Resolve(Bar);
	 */
	public Resolve(type: new(...args:any[]) => any): any {
		// check for injections
		if (this.services.has(type))
			return this.services.get(type);

		// get parameter types
		let params = Reflect.getMetadata('design:paramtypes', type);

		// parameterless constructor available
		if (!params) return new type();
		
		// prepare argument list
		let args :(new(...args:any[])=>any)[] = [];

		// iterate required parameters
		params.forEach((param: new(...args:any[]) => any) => {
			// throw exception if strict and parameter type was not injected
			// otherwise create instances recoursively using default values. (0, null)
			if (this.settings.strict && !this.services.has(param))
				throw `unable to resolve the ${param.name} parameter of the ${type.name} service`;
			// resolve parameter and add to the argument list
			args.push(this.Resolve(param));
		});
		// construct instance with resolved arguments
		return new type(...args);
	}
}

/**
 * Provides a shared context for injected services
 */
export const DefaultInjector = new Injector();