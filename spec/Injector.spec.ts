/**
 * Unit Test file for the Typescript Injection Library
 * @license "MIT Licence"
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * @copyright Silvan Pfister 2019
 * @author Silvan Pfister
 * @version 1.0
 */

import { Injector, DefaultInjector } from "../src/Injector";
import { Service } from "../src/ServiceDecorator";

// #region Test Class Definition

@Service()
class FooService{

	public InjectedNum:number

	constructor(num:number){
		this.InjectedNum = num;
	}
	public get BaseMethod():number{
		return 2;
	}
}


@Service()
class BarService{
	public Text:String = "Unchanged";
	constructor() { }
}

@Service()
class FooBarService{
	public Foo:FooService;
	public Bar:BarService;
	constructor(fooParameter:FooService, barParameter:BarService) {
		this.Foo = fooParameter;
		this.Bar = barParameter;
	}
	public get Method():string{
		return "Unchanged";
	}
}

class FooBarServiceImplementation extends FooBarService{
	public get Method():string{
		return "Changed";
	}
}

@Service("Changed")
class ServiceConstructorTest {

	public Value:string = "Unchanged";

	constructor(value:string) {
		this.Value = value;
	}
}

// #endregion

describe('injector test suite', () => {

	it('should be able to resolve non-strict', () => {
		expect(() => DefaultInjector.Resolve(FooBarService)).not.toThrow();
	});
	
	it('should resolve default types correctly', ()=>{
		let instance:FooBarService = DefaultInjector.Resolve(FooBarService);

		expect(instance.Method).toEqual("Unchanged");
		expect(instance.Foo).toBeDefined();
		expect(instance.Foo.BaseMethod).toEqual(2);
		expect(instance.Foo.InjectedNum).toEqual(0);
		expect(instance.Bar.Text).toEqual("Unchanged");
	});

	it('should provide the service constructor values correctly', ()=>{
		let instance:ServiceConstructorTest = DefaultInjector.Resolve(ServiceConstructorTest);
		expect(instance.Value).toEqual("Changed");
	})

	it('should always provide the same instances in the same context', ()=>{
		let instance1:FooBarService = DefaultInjector.Resolve(FooBarService);
		let instance2:FooBarService = DefaultInjector.Resolve(FooBarService);

		expect(instance1).toBe(instance2);
	});

	it('should provide a different instance in different contexts', ()=>{
		let customInjector = new Injector();

		customInjector.Inject(FooBarService,null);

		let instance1:FooBarService = DefaultInjector.Resolve(FooBarService);
		let instance2:FooBarService = customInjector.Resolve(FooBarService);
		let instance3:FooBarService = customInjector.Resolve(FooBarService);

		expect(instance1).not.toBe(instance2);
		expect(instance2).toBe(instance3);
	})

	it('should provide different instances only if the context is missing the injection', ()=>{
		let customInjector = new Injector();

		customInjector.Inject(FooService, null);

		let instance1:FooBarService = customInjector.Resolve(FooBarService);
		let instance2:FooBarService = customInjector.Resolve(FooBarService);

		expect(instance1).not.toBe(instance2);
		expect(instance1.Foo).toBe(instance2.Foo);
	})

	it('should be able to resolve custom implementations', () => {
		let customInjector = new Injector();

		customInjector.Inject(FooService, new FooService(5));
		customInjector.Inject(FooBarService, customInjector.Resolve(FooBarServiceImplementation));

		let instance:FooBarService = customInjector.Resolve(FooBarService);

		expect(instance.Method).toEqual("Changed");
		expect(instance.Foo.InjectedNum).toEqual(5);
	});

	it('should throw an error when a service constructor cannot be resolved on strict mode', () =>{
		let customInjector = new Injector();
		customInjector.settings.strict = true;

		expect(() => {
			customInjector.Resolve(FooBarService);
		}).toThrow(`unable to resolve the ${FooService.name} parameter of the ${FooBarService.name} service`);

		customInjector.Inject(FooService, new FooService(1));
		
		expect(() => {
			customInjector.Resolve(FooBarService);
		}).toThrow(`unable to resolve the ${BarService.name} parameter of the ${FooBarService.name} service`);

		customInjector.Inject(BarService, new BarService());

		expect(() => {
			customInjector.Resolve(FooBarService);
		}).not.toThrow();
	})
});
